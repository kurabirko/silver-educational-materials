## Qworld's Silver (Under development)

Welcome to initial draft of QWorld's Silver. The tutorial is under development.

Please make your suggestions and corrections using the Google Doc link below.

https://docs.google.com/document/d/1MEYfwxiz0EJpGwzh8P0VCVhy_Ou53FfJBTapLop2VYs/edit?usp=sharing

## Credits

Most of the Silver is prepared under the project QPool2019 GitLab repository in 2019-2020.

First Quantum Algorithms part is developed by a group of [QTurkey](https://www.qturkey.org) members. The contributors are Arda Çınar, Berat Yenilen, Cenk Tüysüz, [Dr. Özlem Salehi](https://www.cmpe.boun.edu.tr/~ozlem.salehi/), and Utku Birkan. Forest SDK and Solving Max-Cut problem using Grover's Search section is prepared by [Adam Glos](https://iitis.pl/en/person/aglos) (IITiS PAN, [QPoland](http://qworld.lu.lv/index.php/qpoland/)). Introduction to Complex Numbers section is developed by Dr. Maksim Dimitrijev ([QLatvia](http://qworld.lu.lv/index.php/qlatvia/)). Quantum Fourier Transform and Shor's Algorithm section is developed by Dr. Özlem Salehi.

## Making Contributions

If you are interested, you are welcome to contribute to Silver. Please read QWorld's rules for developing projects.

http://qworld.lu.lv/wp-content/uploads/2020/09/Rules-for-the-projects-developed-under-the-QEducation-2020-Sep-22.pdf


## License


The text and figures are licensed under the Creative Commons Attribution 4.0 International Public License (CC-BY-4.0), available at https://creativecommons.org/licenses/by/4.0/legalcode.

The code snippets in the notebooks are licensed under Apache License 2.0, available at http://www.apache.org/licenses/LICENSE-2.0.